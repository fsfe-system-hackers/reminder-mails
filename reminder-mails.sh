#!/bin/bash

# This file takes care of identifying the requested mail, adding necessary
# information to the template, and piping it to msmtp to send it out.
#
# Copyright (c) 2018 Free Software Foundation Europe <contact@fsfe.org>
# Author 2018 Max Mehl <max.mehl@fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later

prepmail () {
  # read mail in variable
  mail=$(< "$1")

  # get dynamic variables from file
  while read -r line; do
    if [[ -n "$line" ]]; then
      # remove prefix
      line=${line//# var: /}
      # export the line in the format of var=value
      eval "export $line"
    fi
  done <<< "$(grep '^# var: ' <<< "$mail")"

  # add date and message ID in the first lines
  # and remove all local variable declaration from template
  mail=$(echo "$mail" | sed "1s|^|Date: $(date -R)\n|" \
                      | sed "1s|^|Message-ID: <ReMiNdEr.$(date +%s)@fsfe.org>\n|" \
                      | sed '/^#.*/d')

  # replace variables in mail with exported variables
  envsubst <<< "$mail"
}

send_mail() {
  if [[ "$DEBUG" != "debug" ]]; then
    msmtp -t --read-envelope-from -a fsfe
  else
    cat
  fi
}

EMAIL=$1
DEBUG=$2

# If argument is given, check for mail template and send it out
if [[ -n "$EMAIL" ]]; then
  if [[ -e "$EMAIL" ]]; then
    if prepmail "$EMAIL" | send_mail; then
      echo "[INFO] $EMAIL has been sent successfully"
    else
      echo "[ERROR] There has been an error with sending $EMAIL".
      exit 1
    fi
  else
    echo "[ERROR] Mail template $EMAIL does not exist."
    exit 1
  fi
else
  echo "[INFO] No argument given. Will not send a mail."
  exit 0
fi
