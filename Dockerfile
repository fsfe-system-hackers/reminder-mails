# SPDX-FileCopyrightText: 2019 Free Software Foundation Europe e.V.
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Base image selection
FROM bitnami/minideb:bullseye

# Installing the services and dependencies
RUN install_packages msmtp ca-certificates curl gettext-base

# Manually install supercronic
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.12/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=048b95b48b708983effb2e5c935a1ef8483d9e3e
RUN curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

# Set up unprivileged user
RUN adduser --shell "/sbin/nologin" --gecos "Reminder" --disabled-password reminder
USER reminder
WORKDIR /home/reminder

# Add script and mails
COPY reminder-mails.sh cron.txt .msmtprc /home/reminder/
RUN mkdir mails
COPY mails/* /home/reminder/mails/

# CMD to run the cron on container start
CMD supercronic /home/reminder/cron.txt
