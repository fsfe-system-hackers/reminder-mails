<!--
SPDX-FileCopyrightText: 2019 Free Software Foundation Europe e.V.

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Reminder Mails

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/reminder-mails/00_README)
[![Build Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/reminder-mails/status.svg)](https://drone.fsfe.org/fsfe-system-hackers/reminder-mails)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/reminder-mails)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/reminder-mails)

This program sends periodic mails which can be defined by easy mail templates.
The interval for each mail can be defined in `cron` style. Sending works by
using `msmtp`.

## Install

This repository is meant to be installed using Docker and Drone CI. Docker logs
will contain the status of the cron commands.

## Configure templates

The templates shall be plain text files. The must contain at least the following
headers: `To`, `From`, `Subject`. The `Date` and `Message-ID` headers are added
automatically.

You can put comments in the file using `#` as the first character of a line.
These will be deleted before sending.

### Variables

You can add variables to a template by prepending `# var: `. Here is a simple
example:

```
# var: directory="/tmp"
# var: files=$(ls -l "$directory")
To: me <me@example.com>
From: no-reply@example.com
Subject: Regular file listing in ${directory}

Hello. Here are all files in ${directory}:

${files}
```

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/
